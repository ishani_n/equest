<?php require_once 'app/global/url.php' ?>

<!DOCTYPE html>
<html lang="en-US" class="no-js">

<head>
<?php require_once ROOT_PATH.'app/analytics/analytics_head.php';
        require_once ROOT_PATH.'app/meta/meta.php';
        $meta_single_page_title = ' ';
        $meta_single_page_desc = '';
        $meta_arr = array(
            'title' => $meta_single_page_title,
            'description' => $meta_single_page_desc,
            'image' => URL.'assets/images/meta/home.jpg',
            
            'og:title' => $meta_single_page_title,
            'og:image' => URL.'assets/images/meta/home.jpg',
            'og:description' => $meta_single_page_desc,

            'twitter:image' => URL.'assets/images/meta/home.jpg',
            'twitter:title' => $meta_single_page_title,

        );
        require_once ROOT_PATH.'app/meta/meta_more_details.php'; 
    ?>
    <?php require_once 'imports/css.php' ?>


</head>

<body class="home page-template-default page page-id-6208 gdlr-core-body tribe-no-js kingster-body kingster-body-front kingster-full kingster-with-sticky-navigation kingster-sticky-navigation-no-logo kingster-blockquote-style-1 gdlr-core-link-to-lightbox" >
<?php require_once 'imports/mbl_header.php' ?>
    <div class="kingster-body-outer-wrapper">
        <div class="kingster-body-wrapper clearfix kingster-with-transparent-header kingster-with-frame">
            <div class="kingster-header-background-transparent">
            <?php require_once 'imports/header_top.php' ?>
                <?php require_once 'imports/header.php' ?>
                <!-- header -->
            </div>
            <div class="kingster-page-wrapper" id="kingster-page-wrapper">
                <div class="gdlr-core-page-builder-body">
                    <div class="gdlr-core-pbf-wrapper" style="padding: 0px 0px 0px 0px;">
                        <div class="gdlr-core-pbf-background-wrap"></div>
                        <div class="gdlr-core-pbf-wrapper-content gdlr-core-js">
                            <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-pbf-wrapper-full-no-space">
                                <div class="gdlr-core-pbf-element">
                                    <div class="gdlr-core-revolution-slider-item gdlr-core-item-pdlr gdlr-core-item-pdb" style="padding-bottom: 0px;">
                                        <!-- START Homepage College REVOLUTION SLIDER 6.2.22 -->
                                        <?php require_once 'home_include/slider.php' ?>
                                        <!-- END REVOLUTION SLIDER -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="gdlr-core-pbf-section">
                        <div class="gdlr-core-pbf-section-container gdlr-core-container clearfix">
                            <div class="gdlr-core-pbf-column gdlr-core-column-60 gdlr-core-column-first gdlr-core-hide-in-mobile" id="gdlr-core-column-35275" style="z-index: 9;">
                                <div class="gdlr-core-pbf-column-content-margin gdlr-core-js" style="margin: 0px 0px 0px 0px;">
                                    <div class="gdlr-core-pbf-background-wrap"></div>
                                    <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js">
                                        <div class="gdlr-core-pbf-element">
                                            <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-item-pdlr gdlr-core-center-align gdlr-core-style-vertical" style="margin-bottom: 50px;">
                                                <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #d1d1d1; height: 100px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php require_once 'home_include/about.php' ?>
                    <?php require_once 'home_include/service.php' ?>
                    <div class="gdlr-core-pbf-wrapper" style="padding: 175px 0px 100px 0px;">
                        <div class="gdlr-core-pbf-background-wrap  exmple-height">
                            <div
                                class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js"
                                style="background-image: url(assets/img/back.jpg); background-size: cover; background-position: center;"
                                
                            ></div>
                        </div>
                        <div class="gdlr-core-pbf-wrapper-content gdlr-core-js">
                            <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                                <div class="gdlr-core-pbf-column gdlr-core-column-20 gdlr-core-column-first">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
                                                    <div class="gdlr-core-title-item-title-wrap clearfix">
                                                        <h1 class="banner-font " style="">
                                                            CAMPUS <br> AMBASSADOR
                                                        </h1>
                                                        <h1 class="banner-font-2" >
                                                            PROGRAMMER
                                                        </h1>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              
                            </div>
                        </div>
                    </div>
                    <?php require_once 'home_include/benifits.php' ?>

                  
                    <?php require_once 'home_include/banner.php' ?>



                    <?php require_once 'home_include/faq.php' ?>


                    <?php require_once 'home_include/banner_2.php' ?>


                    <?php require_once 'home_include/contact.php' ?>























                  
                   
                  
                    
                  
                   
                 

                  
                </div>
            </div>
            <?php require_once 'imports/footer.php' ?>
            <!-- The Modal -->
           
<!-- Modal -->
<div class="modal fade bd-example-modal-lg model-one-1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="row" style="padding: 20px;">
            <div class="col-md-4">
                <img src="assets/img/Dinisu.png">
            </div>
            <div class="col-md-8">
                <h4>Dinisu Indrachapa</h4>
                <span>Founder and CEO – Equest Institute of Higher Education </span>
                <p>A Digital Marketing and ecommerce professional with over 8 years of experience, Dinisu is one of the earliest adopters to the digital transformation wave in the profession of marketing. He is one of the most experienced digital strategists in Sri Lanka, and spearheaded digital strategy for some leading local and International brands.</p>
                <p>Started a mission to develop the Sri Lankan economy in 2017 via online entrepreneurship by starting his own company; eClub International (Pvt) Ltd. After few years he stepped-out from his own company with a new vision, new goal and with a fresh new team. Within a small time of one and half years he opened 4 branches in Galle, Kurunegala ,Malabe and Panadura respectively by the name of Equest Institute of higher education which now is the best in business. By that time, he presented various types of income sources and in the near future more sources will be introduced to each and every youngster to uplift their lifestyle. </p>
            </div>
        </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade bd-example-modal-lg model-two-2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="row" style="padding: 20px;">
            <div class="col-md-4">
                <img src="assets/img/Dinisu.png">
            </div>
            <div class="col-md-8">
                <h4>Nilanga Sandaruwan</h4>
                <span>Financial Director – Equest Institute of Higher Education</span>
                <p>Nilanga Sandaruwan is director of finance at Equest Institute of Higher Education. He is a strategic leader who has headed our global finance function since the beginning of the company. Nilanga had been graduated with B.Sc in Accounting and Finance Management from the Eastern University of Sri Lanka. He started his career at the Central Bank of Sri Lanka. He provides his knowledge and working in dynamic and high-growth companies includes NSBM, Arpico Finance, and Hikka Tranz. During his 3-year (2019) tenure at Equest Institute of Higher Education, Nilanga has had extensive experience working around the globe as a client service partner, client finance advisor, and prominent as a Financial Director.</p>
            </div>
        </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade bd-example-modal-lg model-threee-3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="row" style="padding: 20px;">
            <div class="col-md-4">
                <img src="assets/img/Dinisu.png">
            </div>
            <div class="col-md-8">
                <h4>Romesh Samarakoon </h4>
                <span>Administrative Director – Equest Institute of Higher Education</span>
                <p>Romesh Samarakoon is the director of Administration at Equest Institute of Higher Education. He is a strategic leader since the beginning of the company. He started his marketing carrier in 2014 and now he is a marketing professional with over 8 years of experience. Apart from that, He is a Digital marketing & eCommerce professional with more than 5 years of experience. Romesh Samarakoon participate for many foreign trainings which helped him to build up more skills and new tactics and which later lead him to become a successful motivational speaker who now has the immense pleasure of changing the life of many.</p>
            </div>
        </div>
    </div>
  </div>
</div>





        </div>
    </div>

 
    <?php require_once 'imports/js.php' ?>


</body>
</html>