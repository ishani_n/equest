<header class="kingster-header-wrap kingster-header-style-plain kingster-style-splitted-menu kingster-sticky-navigation kingster-style-slide clearfix" data-navigation-offset="75px">
                    <div class="kingster-header-background"></div>
                    <div class="kingster-header-container kingster-container">
                        <div class="kingster-header-container-inner clearfix">
                            <div class="kingster-navigation kingster-item-pdlr clearfix">
                                <div class="kingster-main-menu" id="kingster-main-menu">
                                    <ul id="menu-main-navigation-1" class="sf-menu">
                                        <li class="menu-itemmenu-item-home current-menu-item page_item page-item-6208 current_page_item  kingster-normal-menu">
                                            <a href="<?php echo URL?>">Home</a>
                                        </li>
                                        <li class="menu-item menu-item-has-children  kingster-normal-menu">
                                            <a href="home#gdlr-core-wrapper-1" class="sf-with-ul-pre">Value</a>
                                            
                                        </li>
                                        <li class="menu-itemmenu-item-has-children  kingster-mega-menu">
                                            <a href="home#benefits" class="sf-with-ul-pre">Benefits</a>
                                           
                                        </li>
                                        
                                        <li class="kingster-center-nav-menu-item">
                                            <div class="kingster-logo kingster-item-pdlr">
                                                <div class="kingster-logo-inner">
                                                    <a class="" href="<?php echo URL?>"><img src="<?php echo URL?>assets/img/logo.png" alt="" width="200" height="200" title="logo HP" /></a>
                                                </div>
                                            </div>
                                        </li>
                                       
                                        <li class="menu-item kingster-normal-menu"><a href="home#structure">Structure</a></li>
                                        <li class="menu-item kingster-normal-menu"><a href="<?php echo URL?>course">Courses</a></li>
                                        <li class="menu-itemmenu-item-has-children  kingster-normal-menu">
                                            <a href="home#conatct" class="sf-with-ul-pre">Contact Us</a>
                                           
                                        </li>
                                    </ul>
                                </div>
                          
                            </div>
                            <!-- kingster-navigation -->
                        </div>
                        <!-- kingster-header-inner -->
                    </div>
                    <!-- kingster-header-container -->
                </header>