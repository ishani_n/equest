
    <link rel="stylesheet" href="assets/css/common-skeleton.min.css?" type="text/css" media="all" />
    <link rel="stylesheet" href="assets/css/tooltip.min.css" type="text/css" media="all" />
    <link rel="stylesheet" href="assets/css/font-awesome.css" type="text/css" media="all" />
    <link rel="stylesheet" href="assets/css/elegant-font.css" type="text/css" media="all" />
    <link rel="stylesheet" href="assets/css/style.css" type="text/css" media="all" />
    <link rel="stylesheet" href="assets/css/page-builder.css" type="text/css" media="all" />
    <link rel="stylesheet" href="assets/plugins/revslider/public/assets/css/rs6.css" type="text/css" media="all" />
    <link rel="stylesheet" href="assets/css/style-core.css" type="text/css" media="all" />
    <link rel="stylesheet" href="assets/css/kingster-style-custom.css" type="text/css" media="all" />
    <link rel="stylesheet" href="assets\css\style_new.css" type="text/css" media="all" />
    <link rel="stylesheet" href="assets\css\responsive.css" type="text/css" media="all" />
<!-- CSS only -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <!-- https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css -->
    <link
        rel="stylesheet"
        id="gdlr-core-google-font-css"
        href="https://fonts.googleapis.com/css?family=Jost%3A100%2C200%2C300%2Cregular%2C500%2C600%2C700%2C800%2C900%2C100italic%2C200italic%2C300italic%2Citalic%2C500italic%2C600italic%2C700italic%2C800italic%2C900italic%7CMontserrat%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2Cregular%2Citalic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&amp;subset=cyrillic%2Clatin%2Clatin-ext%2Ccyrillic-ext%2Cvietnamese&amp;ver=5.5.1"
        type="text/css"
        media="all"
    />
    <link href="https://fonts.googleapis.com/css?family=Poppins:400%2C700%7CRoboto:400" rel="stylesheet" property="stylesheet" media="all" type="text/css" />