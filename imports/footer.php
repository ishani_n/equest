<footer class="kingster-no-title-divider footer-background-color">
                <div class="kingster-footer-wrapper footer-background-color">
                    <div class="kingster-footer-container kingster-container clearfix">
                        <div class="kingster-footer-column kingster-item-pdlr kingster-column-60">
                        <div class="kingster-navigation kingster-item-pdlr clearfix">
                                <div class="kingster-main-menu flex-footer" id="kingster-main-menu">
                                    <ul id="menu-main-navigation-1" class="sf-menu">
                                        <li class="menu-itemmenu-item-home current-menu-item page_item page-item-6208 current_page_item  kingster-normal-menu">
                                            <a class="footer-link" href="<?php echo URL?>">Home</a>
                                        </li>
                                        <li class="menu-item menu-item-has-children  kingster-normal-menu">
                                            <a href="home#gdlr-core-wrapper-1" class="footer-link sf-with-ul-pre">Value</a>
                                            
                                        </li>
                                        <li class="menu-itemmenu-item-has-children  kingster-mega-menu">
                                            <a href="home#benefits" class="footer-link sf-with-ul-pre">Benefits</a>
                                           
                                        </li>
                                        
                                        <li class="kingster-center-nav-menu-item">
                                            <div class="kingster-logo kingster-item-pdlr kingster-logo-footer">
                                                <div class="kingster-logo-inner">
                                                    <a class="" href="<?php echo URL?>"><img class="" src="<?php echo URL?>assets/img/footer.png" alt="" width="200" height="200" title="logo HP" /></a>
                                                </div>
                                            </div>
                                        </li>
                                       
                                        <li class="menu-item kingster-normal-menu"><a class="footer-link" href="home#structure">Structure</a></li>
                                        <li class="menu-item kingster-normal-menu"><a class="footer-link" href="<?php echo URL?>course">Courses</a></li>
                                        <li class="menu-itemmenu-item-has-children  kingster-normal-menu">
                                            <a href="home#conatct" class="footer-link sf-with-ul-pre">Contact</a>
                                           
                                        </li>
                                    </ul>
                                </div>
                          
                            </div>
                        
                        </div>
                      
                       
                    </div>
                </div>
                <div class="kingster-copyright-wrapper footer-background-color">
                    <div class="kingster-copyright-container kingster-container clearfix border-footer">
                        <div class="kingster-copyright-left kingster-item-pdlr gdlr-core-column-50" style="color: #192f59;text-align: center;">Equest Copyright  2021.All Right Reserved. Web solutions by <a target="_blank" href="https://www.creativehub.global/" style="color:#192f59;">Creative<span style="color:orange;">Hub</span></a></div>
                        <div class="kingster-copyright-right kingster-item-pdlr gdlr-core-column-10">
                            <div class="gdlr-core-social-network-item gdlr-core-item-pdb gdlr-core-none-align text-right" style="padding-bottom: 0px;">
                                <a href="https://www.facebook.com/equest.lk" target="_blank" class="gdlr-core-social-network-icon " title="facebook"><i class="fa fa-facebook scocial-icon"></i></a>
                                <!-- <a href="#" target="_blank" class="gdlr-core-social-network-icon" title="linkedin"><i class="fa fa-linkedin scocial-icon"></i></a> -->
                            </div>
                        </div>
                    </div>
                </div>
            </footer>


          