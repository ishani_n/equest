<div class="gdlr-core-pbf-wrapper" style="padding: 0px 0px 100px 0px;" id="gdlr-core-wrapper-1">
                        <div class="gdlr-core-pbf-background-wrap"></div>
                        <div class="gdlr-core-pbf-wrapper-content gdlr-core-js">
                            <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                                <div class="gdlr-core-pbf-column gdlr-core-column-60 gdlr-core-column-first" id="gdlr-core-column-19513">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js" style="padding-bottom: 80px;">
                                        <div class="gdlr-core-pbf-background-wrap"></div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js" style="max-width: 700px;">
                                            <div class="gdlr-core-pbf-element">
                                                <div
                                                    class="gdlr-core-title-item gdlr-core-item-pdb clearfix gdlr-core-center-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr"
                                                    style="padding-bottom: 28px;"
                                                    id="gdlr-core-title-item-1" >
                                                   
                                                    <div class="gdlr-core-title-item-title-wrap clearfix">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title" style="font-size: 42px; font-weight: 400; color: #222222;">Do you dream of being in charge of your own destiny?</h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-center-align gdlr-core-no-p-space" style="padding-bottom: 48px;">
                                                    <div class="gdlr-core-text-box-item-content" style="font-size: 18px; font-weight: 400; letter-spacing: 0px; text-transform: none; color: #373737;">
                                                        <p>
                                                        Being an entrepreneur gives you a sense of freedom and empowerment. You can build things and watch them grow. It's a great way to live. 
                                                        </p>
                                                        <p>
                                                        But succeeding as an entrepreneur doesn't come overnight. It takes a lot of planning, preparation. creativity. grit, and patience to become a successful entrepreneur. 
                                                        </p>
                                                        <p>
                                                        Equest Institute of Higher Education intends to provide the first changes in the skills and mindset of students and teach company management skills to promote the development of an entrepreneurial culture.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-button-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-center-align" style="padding-bottom: 0px;">
                                                    <a
                                                        class="gdlr-core-button gdlr-core-button-transparent gdlr-core-center-align gdlr-core-button-with-border"
                                                        href="#"
                                                        style="
                                                            font-size: 13px;
                                                            font-weight: 600;
                                                            letter-spacing: 1px;
                                                            color: #777777;
                                                            padding: 0px 0px 2px 0px;
                                                            text-transform: uppercase;
                                                            border-radius: 0px;
                                                            -moz-border-radius: 0px;
                                                            -webkit-border-radius: 0px;
                                                            border-width: 0px 0px 1px 0px;
                                                            border-color: #777777;
                                                        "
                                                    >
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>