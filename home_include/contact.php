<div class="gdlr-core-pbf-wrapper">
                            <div class="gdlr-core-pbf-wrapper-content gdlr-core-js" id="conatct">
                                <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                                    <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first">
                                        <div class="gdlr-core-pbf-column-content-margin gdlr-core-js">
                                            <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js">
                                                <div class="gdlr-core-pbf-element">
                                                    <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 35px;">
                                                        <div class="gdlr-core-title-item-title-wrap clearfix">
                                                            <h3 class="gdlr-core-title-item-title gdlr-core-skin-title" style="font-size: 24px; font-weight: 700; letter-spacing: 0px; text-transform: none; color: #1a1a1a;">
                                                                Contact Us
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="gdlr-core-pbf-element">
                                                    <div class="mb-40 gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 15px;">
                                                        <div style="display: flex;">
                                                            <div class="detail-flex">
                                                                <img class="conatct-logo" src="assets/img/logo_blue.png">
                                                            </div>
                                                            <div>
                                                                <h4>EQUEST BRANCH 1</h4>
                                                                <p>
                                                                420, 4th floor,<br> No 10, Wakwella Road,<br> Galle
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mb-40 gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 15px;">
                                                        <div style="display: flex;">
                                                            <div class="detail-flex">
                                                                <img class="conatct-logo" src="assets/img/logo_blue.png">
                                                            </div>
                                                            <div>
                                                            <h4>EQUEST BRANCH 2</h4>
                                                                <p>
                                                                No 395/2/C, <br>Kaduwela Road,<br> Malabe
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mb-40 gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 15px;">
                                                        <div style="display: flex;">
                                                        <div class="detail-flex">
                                                                <img class="conatct-logo" src="assets/img/logo_blue.png">
                                                            </div>
                                                            <div>
                                                            <h4>EQUEST BRANCH 3</h4>
                                                                <p>
                                                                No.22, Puttalam Road,<br> Yanthampalawa,<br> Kurunegala
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mb-40 gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 15px;">
                                                        <div style="display: flex;">
                                                        <div class="detail-flex">
                                                                <img class="conatct-logo" src="assets/img/logo_blue.png">
                                                            </div>
                                                            <div>
                                                            <h4>EQUEST BRANCH 4</h4>
                                                                <p>
                                                                27/ ½, <br>7th Cross Street,<br> Panadura
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                               
                                             
                                               
                                            </div>
                                        </div>
                                    </div>
                                    <div class="gdlr-core-pbf-column gdlr-core-column-30" data-skin="Green Form" id="gdlr-core-column-56596">
                                        <div class="gdlr-core-pbf-column-content-margin gdlr-core-js">
                                            <div class="gdlr-core-pbf-background-wrap"></div>
                                            <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js">
                                                <div class="gdlr-core-pbf-element">
                                                    <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 35px;">
                                                        <div class="gdlr-core-title-item-title-wrap clearfix">
                                                            <h3 class="gdlr-core-title-item-title gdlr-core-skin-title" style="font-size: 24px; font-weight: 700; letter-spacing: 0px; text-transform: none; color: #1a1a1a;">
                                                                Make an appointment
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="gdlr-core-pbf-element">
                                                    <div class="gdlr-core-contact-form-7-item gdlr-core-item-pdlr gdlr-core-item-pdb">
                                                        <div role="form" class="wpcf7" id="wpcf7-f5439-p5433-o1" lang="en-US" dir="ltr">
                                                            <div class="screen-reader-response" role="alert" aria-live="polite"></div>
                                                                <form>
                                                                    <div class="form-row mb-40">
                                                                        <div class="col">
                                                                            <input type="text" class="form-control" placeholder="Name">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row mb-40">
                                                                        <div class="col">
                                                                            <input type="text" class="form-control" placeholder="Email">
                                                                        </div>
                                                                        <div class="col">
                                                                            <input type="text" class="form-control" placeholder="Phone Number">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row mb-40">
                                                                        <div class="col">
                                                                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <div class="col">
                                                                            <button  type="submit" class="submit-button float-left"><span>Contact Us</span></button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>