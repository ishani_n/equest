<div class="gdlr-core-pbf-wrapper" id="benefits" style="padding: 0px 0px 35px 0px;">
                        <div class="gdlr-core-pbf-background-wrap" style="background-color: #f3f3f3;"></div>
                        <div class="gdlr-core-pbf-wrapper-content gdlr-core-js">
                            <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container box-padding">
                                <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first" id="gdlr-core-column-40608">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js" style="margin-top: -100px; margin-right: 15px; margin-left: 15px; padding-left: 0px;">
                                        <div class="gdlr-core-pbf-background-wrap"></div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-image-item gdlr-core-item-pdb gdlr-core-center-align" style="padding-bottom: 25px;">
                                                    <div class="gdlr-core-image-item-wrap gdlr-core-media-image gdlr-core-image-item-style-rectangle" style="border-width: 0px;">
                                                        <img src="<?php echo URL?>assets/img/a.jpg" alt="" width="640" height="449" title="Img1" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
                                                    <div class="gdlr-core-title-item-title-wrap clearfix">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title" style="font-size: 22px; font-weight: 400; letter-spacing: 0px; color: #222222;">Who we are </h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align gdlr-core-no-p-space" style="padding-bottom: 25px; margin-left: 0px;">
                                                    <div class="gdlr-core-text-box-item-content" style="font-size: 18px; font-weight: 400; text-transform: none; color: #696969;">
                                                        <p>Equest International is the only higher education institute in Sri Lanka which builds entrepreneurs with various skills for the 21st century. </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-button-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 50px;">
                                                    <a
                                                        class="gdlr-core-button gdlr-core-button-transparent gdlr-core-left-align gdlr-core-button-no-border"
                                                        href="#"
                                                        style="
                                                            font-size: 13px;
                                                            font-weight: 600;
                                                            letter-spacing: 1px;
                                                            color: #777777;
                                                            padding: 0px 0px 0px 0px;
                                                            text-transform: uppercase;
                                                            border-radius: 0px;
                                                            -moz-border-radius: 0px;
                                                            -webkit-border-radius: 0px;
                                                        "
                                                    >
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30" id="gdlr-core-column-50951">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js" style="margin-top: -100px; margin-right: 15px; margin-left: 15px; padding-left: 0px;">
                                        <div class="gdlr-core-pbf-background-wrap"></div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-image-item gdlr-core-item-pdb gdlr-core-center-align" style="padding-bottom: 25px;">
                                                    <div class="gdlr-core-image-item-wrap gdlr-core-media-image gdlr-core-image-item-style-rectangle" style="border-width: 0px;">
                                                        <img src="<?php echo URL?>assets/img/b.jpg" alt="" width="640" height="449" title="Img2" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
                                                    <div class="gdlr-core-title-item-title-wrap clearfix">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title" style="font-size: 22px; font-weight: 400; letter-spacing: 0px; color: #222222;">Why select Equest? </h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align gdlr-core-no-p-space" style="padding-bottom: 25px; margin-left: 0px;">
                                                    <div class="gdlr-core-text-box-item-content" style="font-size: 18px; font-weight: 400; text-transform: none; color: #696969;">
                                                        <p>At Equest Institute of Higher Education, we know what it really takes to be an entrepreneur and build a business from scratch, what personal characteristics do successful entrepreneurs share and what types of resources are available to budding entrepreneurs, and where can you find them. </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-button-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 50px;">
                                                    <a
                                                        class="gdlr-core-button gdlr-core-button-transparent gdlr-core-left-align gdlr-core-button-no-border"
                                                        href="#"
                                                        style="
                                                            font-size: 13px;
                                                            font-weight: 600;
                                                            letter-spacing: 1px;
                                                            color: #777777;
                                                            padding: 0px 0px 0px 0px;
                                                            text-transform: uppercase;
                                                            border-radius: 0px;
                                                            -moz-border-radius: 0px;
                                                            -webkit-border-radius: 0px;
                                                        "
                                                    >
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first" id="gdlr-core-column-41511">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js" style="margin-right: 15px; margin-left: 15px; padding-left: 0px;">
                                        <div class="gdlr-core-pbf-background-wrap"></div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-image-item gdlr-core-item-pdb gdlr-core-center-align" style="padding-bottom: 25px;">
                                                    <div class="gdlr-core-image-item-wrap gdlr-core-media-image gdlr-core-image-item-style-rectangle" style="border-width: 0px;">
                                                        <img src="<?php echo URL?>assets/img/c.jpg" alt="" width="640" height="449" title="Img 3" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
                                                    <div class="gdlr-core-title-item-title-wrap clearfix">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title" style="font-size: 22px; font-weight: 400; letter-spacing: 0px; color: #222222;">How we build entrepreneurs</h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align gdlr-core-no-p-space" style="padding-bottom: 25px; margin-left: 0px;">
                                                    <div class="gdlr-core-text-box-item-content" style="font-size: 18px; font-weight: 400; text-transform: none; color: #696969;">
                                                        <p>We provide learners the real-world knowledge they need to create and develop a new enterprise with potential for growth and funding.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-button-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 50px;">
                                                    <a
                                                        class="gdlr-core-button gdlr-core-button-transparent gdlr-core-left-align gdlr-core-button-no-border"
                                                        href="#"
                                                        style="
                                                            font-size: 13px;
                                                            font-weight: 600;
                                                            letter-spacing: 1px;
                                                            color: #777777;
                                                            padding: 0px 0px 0px 0px;
                                                            text-transform: uppercase;
                                                            border-radius: 0px;
                                                            -moz-border-radius: 0px;
                                                            -webkit-border-radius: 0px;
                                                        "
                                                    >
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30" id="gdlr-core-column-69824">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js" style="margin-right: 15px; margin-left: 15px; padding-left: 0px;">
                                        <div class="gdlr-core-pbf-background-wrap"></div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-image-item gdlr-core-item-pdb gdlr-core-center-align" style="padding-bottom: 25px;">
                                                    <div class="gdlr-core-image-item-wrap gdlr-core-media-image gdlr-core-image-item-style-rectangle" style="border-width: 0px;">
                                                        <img src="<?php echo URL?>assets/img/d.jpg" alt="" width="640" height="449" title="Img 4" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
                                                    <div class="gdlr-core-title-item-title-wrap clearfix">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title" style="font-size: 22px; font-weight: 400; letter-spacing: 0px; color: #222222;">Be a part of the Equest family!</h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align gdlr-core-no-p-space" style="padding-bottom: 25px; margin-left: 0px;">
                                                    <div class="gdlr-core-text-box-item-content" style="font-size: 18px; font-weight: 400; text-transform: none; color: #696969;">
                                                        <p>If you have dreams of breaking out of the daily grind and enjoying the many advantages of entrepreneurship. now could be your time! </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-button-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 50px;">
                                                    <a
                                                        class="gdlr-core-button gdlr-core-button-transparent gdlr-core-left-align gdlr-core-button-no-border"
                                                        href="#"
                                                        style="
                                                            font-size: 13px;
                                                            font-weight: 600;
                                                            letter-spacing: 1px;
                                                            color: #777777;
                                                            padding: 0px 0px 0px 0px;
                                                            text-transform: uppercase;
                                                            border-radius: 0px;
                                                            -moz-border-radius: 0px;
                                                            -webkit-border-radius: 0px;
                                                        "
                                                    >
                                                        
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>