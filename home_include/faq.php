<div class="gdlr-core-pbf-wrapper" id="structure" style="padding: 0px 0px 90px 0px;">
                            <div class="gdlr-core-pbf-background-wrap"></div>
                            <div class="gdlr-core-pbf-wrapper-content gdlr-core-js">
                                <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">

                                <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first">
                                        <div class="gdlr-core-pbf-column-content-margin gdlr-core-js">
                                        <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
                                                    <!-- <span
                                                        class="gdlr-core-title-item-caption gdlr-core-info-font gdlr-core-skin-caption"
                                                        style="font-size: 15px; font-style: normal; letter-spacing: 1px; text-transform: uppercase; color: #999999;"
                                                    >
                                                    STRUCTURE
                                                    </span> -->
                                                    <div class="gdlr-core-title-item-title-wrap clearfix">
                                                        <h4 class="gdlr-core-title-item-title gdlr-core-skin-title" style="font-size: 30px; font-weight: 400; letter-spacing: 0px; color: #003263;">
                                                        What you will receive once you complete the Course 
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
                                                    <span
                                                        class="gdlr-core-title-item-caption gdlr-core-info-font gdlr-core-skin-caption"
                                                        style="font-size: 13px; font-style: normal; letter-spacing: 1px; text-transform: uppercase; color: #222222;"
                                                    >
                                                    QUALIFICATION 
                                                    </span>
                                                    <div class="gdlr-core-title-item-title-wrap clearfix">
                                                        <p class="gdlr-core-title-item-title gdlr-core-skin-title" style="font-size: 18px; font-weight: 400; letter-spacing: 0px; color: #999999;text-transform: none;">
                                                        The good thing here is the fact that all you need is a basic knowledge in IT and English! 

                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
                                                    <span
                                                        class="gdlr-core-title-item-caption gdlr-core-info-font gdlr-core-skin-caption"
                                                        style="font-size: 13px; font-style: normal; letter-spacing: 1px; text-transform: uppercase; color: #222222;"
                                                    >
                                                    DURATION 
                                                    </span>
                                                    <div class="gdlr-core-title-item-title-wrap clearfix">
                                                        <p class="gdlr-core-title-item-title gdlr-core-skin-title" style="font-size: 18px; font-weight: 400; letter-spacing: 0px; color: #999999;text-transform: none;">
                                                        The course is conducted 4 days per week (7 hours/day). This means there's 28 hours of training per week. The training includes unlimited revision + practical tutoring + lifetime membership & support! 
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr">
                                                    <span
                                                        class="gdlr-core-title-item-caption gdlr-core-info-font gdlr-core-skin-caption"
                                                        style="font-size: 13px; font-style: normal; letter-spacing: 1px; text-transform: uppercase; color: #222222;"
                                                    >
                                                    APPLY FOR IT NOW 

                                                    </span>
                                                    <div class="gdlr-core-title-item-title-wrap clearfix">
                                                        <p class="gdlr-core-title-item-title gdlr-core-skin-title" style="font-size: 18px; font-weight: 400; letter-spacing: 0px; color: #999999;text-transform: none;">
                                                        What are you waiting for? Apply now and get ready to soar higher than ever! 

                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-button-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align">
                                                    <a class="gdlr-core-button gdlr-core-button-solid gdlr-core-left-align gdlr-core-button-no-border" href="#" style="font-size: 14px ;font-weight: 700 ;letter-spacing: 0px ;padding: 13px 26px 16px 30px;text-transform: none ;margin: 0px 10px 10px 0px;border-radius: 2px;-moz-border-radius: 2px;-webkit-border-radius: 2px;background: #003263 ;"><span class="gdlr-core-content">Apply</span></a>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="gdlr-core-pbf-column gdlr-core-column-30 ">
                                        <div class="gdlr-core-pbf-column-content-margin gdlr-core-js" style="padding: 0px 10px 0px 0px;">
                                            <div class="gdlr-core-pbf-background-wrap"></div>
                                            <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js">
                                                <!-- <div class="gdlr-core-pbf-element">
                                                    <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 35px;">
                                                        <div class="gdlr-core-title-item-title-wrap clearfix">
                                                            <h3 class="gdlr-core-title-item-title gdlr-core-skin-title" style="font-size: 24px; font-weight: 600; letter-spacing: 0px; text-transform: none; color: #1a1a1a;">
                                                                Frequently Asked Questions
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </div> -->
                                                <div class="gdlr-core-pbf-element">
                                                    <div class="gdlr-core-accordion-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-accordion-style-background-title-icon gdlr-core-left-align gdlr-core-icon-pos-right">
                                                        <div class="gdlr-core-accordion-item-tab clearfix gdlr-core-active">
                                                            <div class="gdlr-core-accordion-item-icon gdlr-core-js gdlr-core-skin-icon"></div>
                                                            <div class="gdlr-core-accordion-item-content-wrapper">
                                                                <h4 class="gdlr-core-accordion-item-title gdlr-core-js gdlr-core-skin-e-background gdlr-core-skin-e-content">DAY 1</h4>
                                                                <div class="gdlr-core-accordion-item-content">
                                                                    <p>Introduction</p>
                                                                    <p class="padding-top-faq">Introduction</p>
                                                                    <p class="padding-top-faq">Basic platform layout</p>
                                                                    <p class="padding-top-faq">Direct Shipping</p>
                                                                    <p class="padding-top-faq">Dropshipping</p>
                                                                    <p class="padding-top-faq">Wholesale Supplier Sources</p>
                                                                    <p class="padding-top-faq">Product research strategies 01</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="gdlr-core-accordion-item-tab clearfix">
                                                            <div class="gdlr-core-accordion-item-icon gdlr-core-js gdlr-core-skin-icon"></div>
                                                            <div class="gdlr-core-accordion-item-content-wrapper">
                                                                <h4 class="gdlr-core-accordion-item-title gdlr-core-js gdlr-core-skin-e-background gdlr-core-skin-e-content">DAY 2</h4>
                                                                <div class="gdlr-core-accordion-item-content">
                                                                    <p>Account Setup</p>
                                                                    <p class="padding-top-faq">Finding best Suppliers</p>
                                                                    <p class="padding-top-faq">Product research strategies 02</p>
                                                                    <p class="padding-top-faq">Product Pricing</p>
                                                                    <p class="padding-top-faq">Start selling</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="gdlr-core-accordion-item-tab clearfix">
                                                            <div class="gdlr-core-accordion-item-icon gdlr-core-js gdlr-core-skin-icon"></div>
                                                            <div class="gdlr-core-accordion-item-content-wrapper">
                                                                <h4 class="gdlr-core-accordion-item-title gdlr-core-js gdlr-core-skin-e-background gdlr-core-skin-e-content">DAY 3</h4>
                                                                <div class="gdlr-core-accordion-item-content">
                                                                    <p>Product research strategies 03</p>
                                                                    <p class="padding-top-faq">Tools and Extensions</p>
                                                                    <p class="padding-top-faq">Product listing strategies</p>
                                                                    <p class="padding-top-faq">How to Ship an item</p>
                                                                    <p class="padding-top-faq">Special ebay policies</p>
                                                                    <p class="padding-top-faq">Returns and Refunds Handling</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="gdlr-core-accordion-item-tab clearfix">
                                                            <div class="gdlr-core-accordion-item-icon gdlr-core-js gdlr-core-skin-icon"></div>
                                                            <div class="gdlr-core-accordion-item-content-wrapper">
                                                                <h4 class="gdlr-core-accordion-item-title gdlr-core-js gdlr-core-skin-e-background gdlr-core-skin-e-content">DAY 4</h4>
                                                                <div class="gdlr-core-accordion-item-content">
                                                                    <p>Third party Product Research tools</p>
                                                                    <p class="padding-top-faq">Key points to sell like a Pro</p>
                                                                    <p class="padding-top-faq">Account Managing Strategies</p>
                                                                    <p class="padding-top-faq">Marketing Strategies</p>
                                                                    <p class="padding-top-faq">Customer Handling</p>
                                                                    <p class="padding-top-faq">Top Rated Seller Requirements</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>