<p class="rs-p-wp-fix"></p>
                                        <rs-module-wrap id="rev_slider_1_1_wrapper" data-source="gallery" style="background: transparent; padding: 0; margin: 0px auto; margin-top: 0; margin-bottom: 0;">
                                            <rs-module id="rev_slider_1_1" style="" data-version="6.2.22">
                                                <rs-slides>
                                                    <rs-slide data-key="rs-1" data-title="Slide" data-anim="ei:d;eo:d;s:1000;r:0;t:fade;sl:0;">
                                                        <img
                                                            src="assets/img/ert.jpg"
                                                            title="slider-1"
                                                            width="2000"
                                                            height="1200"
                                                            data-panzoom="d:7000ms;ss:100%;se:103%;"
                                                            class="rev-slidebg"
                                                            data-no-retina
                                                        />
                                                        <!--                        -->
                                                        <rs-layer
                                                            id="slider-1-slide-1-layer-3"
                                                            class="lebeaune-font"
                                                            data-type="text"
                                                            data-rsp_ch="on"
                                                            data-xy="x:c;y:m;yo:18px,18px,18px,88px;"
                                                            data-text="w:normal;s:110,110,110,53;l:100,100,100,37;a:center;"
                                                            data-dim="w:616px,616px,616px,301px;"
                                                            data-frame_1="st:250;sR:250;"
                                                            data-frame_999="o:0;st:w;sR:8450;"
                                                            style="z-index: 10; font-family: Verdana, Geneva, sans-serif;"
                                                        >
                                                            EQUEST
                                                        </rs-layer>
                                                        <!--                        -->
                                                        <rs-layer
                                                            id="slider-1-slide-1-layer-5"
                                                            class="lebeaune-font"
                                                            data-type="text"
                                                            data-rsp_ch="on"
                                                            data-xy="x:c;xo:0,0,0,1px;y:m;yo:-110px,-110px,-110px,131px;"
                                                            data-text="s:22,22,22,16;l:30;ls:0,0,0,1px;fw:400,400,400,700;a:center;"
                                                            data-dim="w:243px,243px,243px,358px;"
                                                            data-frame_1="st:460;sR:460;"
                                                            data-frame_999="o:0;st:w;sR:8240;"
                                                            style="z-index: 11; font-family: Poppins;"
                                                        >
                                                            Welcome to
                                                        </rs-layer>
                                                        <!--                        -->
                                                        <rs-layer
                                                            id="slider-1-slide-1-layer-9"
                                                            class="jost-font"
                                                            data-type="text"
                                                            data-rsp_ch="on"
                                                            data-xy="x:c;xo:16px,16px,16px,0;yo:687px,687px,687px,198px;"
                                                            data-text="w:normal;s:20,20,20,19;l:30;ls:1px;a:center;"
                                                            data-dim="w:505px,505px,505px,358px;h:40px,40px,40px,auto;"
                                                            data-frame_1="st:620;sR:620;"
                                                            data-frame_999="o:0;st:w;sR:8080;"
                                                            style="z-index: 9; font-family: Jost;"
                                                        >
                                                        EQUEST INSTITUTE OF HIGHER EDUCATION
                                                        </rs-layer>
                                                        <!--                        -->
                                                        <a
                                                            id="slider-1-slide-1-layer-14"
                                                            class="rs-layer lebeaune-font"
                                                            href="#"
                                                            target="_self"
                                                            rel="nofollow"
                                                            data-type="text"
                                                            data-rsp_ch="on"
                                                            data-xy="x:c;y:m;yo:347px,347px,347px,129px;"
                                                            data-text="w:normal;s:15,15,15,5;l:25,25,25,9;fw:500;a:center;"
                                                            data-dim="w:182px,182px,182px,67px;"
                                                            data-frame_1="st:990;sR:990;"
                                                            data-frame_999="o:0;st:w;sR:7710;"
                                                            style="z-index: 8; font-family: Jost;"
                                                        >
                                                            Discover Now
                                                        </a>
                                                        <!--                        -->
                                                        <rs-layer
                                                            id="slider-1-slide-1-layer-16"
                                                            data-type="shape"
                                                            data-rsp_ch="on"
                                                            data-xy="x:c;xo:1px,1px,1px,0;yo:931px,931px,931px,348px;"
                                                            data-text="w:normal;s:20,20,20,7;l:0,0,0,9;"
                                                            data-dim="w:1px;h:150px,150px,150px,56px;"
                                                            data-frame_1="st:480;sR:480;"
                                                            data-frame_999="o:0;st:w;sR:8220;"
                                                            style="z-index: 13; background-color: #ffffff;"
                                                        >
                                                        </rs-layer>
                                                        <!--                        -->
                                                        <rs-layer
                                                            id="slider-1-slide-1-layer-21"
                                                            data-type="shape"
                                                            data-rsp_ch="on"
                                                            data-xy="x:c;xo:-4px,-4px,-4px,0;yo:291px,291px,291px,108px;"
                                                            data-text="w:normal;s:20,20,20,7;l:0,0,0,9;"
                                                            data-dim="w:1px;h:100px,100px,100px,37px;"
                                                            data-frame_1="st:480;sR:480;"
                                                            data-frame_999="o:0;st:w;sR:8220;"
                                                            style="z-index: 12; background-color: #ffffff;"
                                                        >
                                                        </rs-layer>
                                                        <!-- -->
                                                    </rs-slide>
                                                </rs-slides>
                                            </rs-module>

                                        </rs-module-wrap>