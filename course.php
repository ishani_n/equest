<?php require_once 'app/global/url.php' ?>

<!DOCTYPE html>
<html lang="en-US" class="no-js">

<head>
<?php require_once ROOT_PATH.'app/analytics/analytics_head.php';
        require_once ROOT_PATH.'app/meta/meta.php';
        $meta_single_page_title = ' ';
        $meta_single_page_desc = '';
        $meta_arr = array(
            'title' => $meta_single_page_title,
            'description' => $meta_single_page_desc,
            'image' => URL.'assets/images/meta/home.jpg',
            
            'og:title' => $meta_single_page_title,
            'og:image' => URL.'assets/images/meta/home.jpg',
            'og:description' => $meta_single_page_desc,

            'twitter:image' => URL.'assets/images/meta/home.jpg',
            'twitter:title' => $meta_single_page_title,

        );
        require_once ROOT_PATH.'app/meta/meta_more_details.php'; 
    ?>
    <?php require_once 'imports/css.php' ?>


</head>

<body class="home page-template-default page page-id-6208 gdlr-core-body tribe-no-js kingster-body kingster-body-front kingster-full kingster-with-sticky-navigation kingster-sticky-navigation-no-logo kingster-blockquote-style-1 gdlr-core-link-to-lightbox" >
<?php require_once 'imports/mbl_header.php' ?>
    <div class="kingster-body-outer-wrapper">
        <div class="kingster-body-wrapper clearfix kingster-with-transparent-header kingster-with-frame">
            <div class="kingster-header-background-transparent">
            <?php require_once 'imports/header_top.php' ?>
                <?php require_once 'imports/header.php' ?>
                <!-- header -->
            </div>
            <div class="kingster-page-wrapper" id="kingster-page-wrapper">
                <div class="gdlr-core-page-builder-body">
                    <div class="gdlr-core-pbf-wrapper" style="padding: 0px 0px 0px 0px;">
                        <div class="gdlr-core-pbf-background-wrap"></div>
                        <div class="gdlr-core-pbf-wrapper-content gdlr-core-js">
                            <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-pbf-wrapper-full-no-space">
                                <div class="gdlr-core-pbf-element">
                                    <div class="gdlr-core-revolution-slider-item gdlr-core-item-pdlr gdlr-core-item-pdb" style="padding-bottom: 0px;">
                                        <!-- START Homepage College REVOLUTION SLIDER 6.2.22 -->
                                        <div class="kingster-page-title-wrap kingster-style-medium kingster-left-align">
                <div class="kingster-header-transparent-substitute">
                </div>
                <div class="kingster-page-title-overlay">
                </div>
                <div class="kingster-page-title-container kingster-container">
                    <div class="kingster-page-title-content kingster-item-pdlr">
                        <div class="kingster-page-caption kingster-center-align">
                            Course code
                        </div>
                        <h1 class="kingster-page-title kingster-center-align">Online Business Course </h1>
                    </div>
                </div>
            </div>
                                        <!-- END REVOLUTION SLIDER -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>










                    <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container padding-course">
                                <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first ">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js ">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 40px;">
                                                    <div class="gdlr-core-title-item-title-wrap clearfix">
                                                        <img src="assets/img/ab.png">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                           
                                          
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30" >
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " >
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js" style="border-bottom: 1px solid;">
                                            
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 40px;">
                                                    <div class="gdlr-core-text-box-item-content" style="text-transform: none;">
                                                        <div style="overflow-x: auto;">
                                                            <table style="min-width: 600px;">
                                                                <tbody>
                                                                    <tr>
                                                                        <th>Course Name</th>
                                                                        <th>Digital Markrting</th>
                                                                        <th>Duration</th>
                                                                        <th>6 hrs</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Course Name</th>
                                                                        <th>Digital Markrting</th>
                                                                        <th>Duration</th>
                                                                        <th>6 hrs</th>
                                                                    </tr>
                                                                   
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                           
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 0px;">
                                                    <div class="gdlr-core-text-box-item-content" style="font-size: 17px; text-transform: none;">
                                                    <h3>What you receive when course complete</h3>
                                                    <p>*will be able to stand up as an online businessmen with a stable active income via successfully launching and maintaining your own business, and earn a minimum of 500 USD monthly income at the end of three months’ time duration by following the instructions and guidelines provided.</p>
                                                    <p>The course is also suitable for entrepreneurs and business owners who wish to set up their online marketing activities in-house.</p>
                                                    </div>
                                                </div>
                                            </div>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                    









                    <div class="gdlr-core-pbf-wrapper " style="padding: 65px 0px 60px 0px;">
                        <div class="gdlr-core-pbf-background-wrap-1" style="background-color: #192f59 ;">
                        </div>
                        <div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
                            <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                                
                                <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first" id="gdlr-core-column-4748">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="margin: -123px 0px 0px 0px;padding: 0px 0px 0px 40px;">
                                        <div class="gdlr-core-pbf-background-wrap-1">
                                        </div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-image-item gdlr-core-item-pdb gdlr-core-center-align gdlr-core-item-pdlr">
                                                    <div class="gdlr-core-image-item-wrap gdlr-core-media-image gdlr-core-image-item-style-rectangle" style="border-width: 0px;">
                                                        <img src="upload/iStock-1152405564-700x450.jpg" alt="" width="700" height="450" title="iStock-1152405564"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30 ">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 45px 0px 0px 0px;">
                                        <div class="gdlr-core-pbf-background-wrap-1">
                                        </div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 20px ;">
                                                    <div class="gdlr-core-text-box-item-content" style="font-size: 23px ;text-transform: none ;color: #ffffff ;">
                                                        <p>
                                                            The PLP in Drafting Legislation, Regulation, and Policy has been offered by the Institute of Advanced Legal Studies with considerable success since 2004.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    


                    <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                                <!-- <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first ">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js ">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 40px;">
                                                    <div class="gdlr-core-title-item-title-wrap clearfix">
                                                        <img src="assets/img/ab.png">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                           
                                          
                                        </div>
                                    </div>
                                </div> -->
                                <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first" style="padding:65px 0px 0px 0px;">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " >
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js">
                                            
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 40px;">
                                                    <div class="gdlr-core-text-box-item-content" style="text-transform: none;">
                                                        <div style="overflow-x: auto;">
                                                            <table style="min-width: 600px;">
                                                                <tbody>
                                                                    <tr>
                                                                        <th>Module</th>
                                                                        <th>Title</th>
                                                                        <th>Duration</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Session 1</td>
                                                                        <td>Introduction & Basic platform layout	</td>
                                                                        <td>6 hrs</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Session 2</td>
                                                                        <td>Account setup & Start selling	</td>
                                                                        <td>7 hrs</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Session 3</td>
                                                                        <td>Advance selling tricks & Product Research 	</td>
                                                                        <td>7 hrs</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Session 4</td>
                                                                        <td>Sales Enhancement Program	</td>
                                                                        <td>7 hrs</td>
                                                                    </tr>
                                                                   
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                           
                                            
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                    
















                            <div class="gdlr-core-pbf-wrapper " style="padding: 20px 0px 60px 0px;">
                        <div class="gdlr-core-pbf-background-wrap" style="background-color: #ffff ;">
                        </div>
                        <div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
                            <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                                <div class="gdlr-core-pbf-element">
                                    <div class="gdlr-core-tab-item gdlr-core-js gdlr-core-item-pdb gdlr-core-left-align gdlr-core-tab-style1-horizontal gdlr-core-item-pdlr">
                                        <!-- <div class="gdlr-core-tab-item-content-image-wrap clearfix">
                                            <div class="gdlr-core-tab-item-image gdlr-core-active" data-tab-id="1">
                                                <a class="gdlr-core-lightgallery gdlr-core-js " href="https://www.youtube.com/watch?v=C5pKtnmHTxg"><span class="gdlr-core-tab-item-image-background" style="background-image: url(upload/tab-1-1.jpg) ;"></span><i class="fa fa-play"></i></a>
                                            </div>
                                            <div class="gdlr-core-tab-item-image " data-tab-id="2">
                                                <a class="gdlr-core-lightgallery gdlr-core-js " href="https://www.youtube.com/watch?v=C5pKtnmHTxg"><span class="gdlr-core-tab-item-image-background" style="background-image: url(upload/tab-2.jpg) ;"></span><i class="fa fa-play"></i></a>
                                            </div>
                                            <div class="gdlr-core-tab-item-image " data-tab-id="3">
                                                <a class="gdlr-core-lightgallery gdlr-core-js " href="https://www.youtube.com/watch?v=C5pKtnmHTxg"><span class="gdlr-core-tab-item-image-background" style="background-image: url(upload/tab-3.jpg) ;"></span><i class="fa fa-play"></i></a>
                                            </div>
                                            <div class="gdlr-core-tab-item-image " data-tab-id="4">
                                                <a class="gdlr-core-lightgallery gdlr-core-js " href="https://www.youtube.com/watch?v=C5pKtnmHTxg"><span class="gdlr-core-tab-item-image-background" style="background-image: url(upload/tab-4.jpg) ;"></span><i class="fa fa-play"></i></a>
                                            </div>
                                        </div> -->
                                        <div class="gdlr-core-tab-item-wrap">
                                            <div class="gdlr-core-tab-item-title-wrap clearfix gdlr-core-title-font">
                                                <div class="gdlr-core-tab-item-title gdlr-core-active" data-tab-id="1">
                                                    Course Delivery
                                                </div>
                                                <div class="gdlr-core-tab-item-title " data-tab-id="2">
                                                    Examination
                                                </div>
                                                <div class="gdlr-core-tab-item-title " data-tab-id="3">
                                                   Enrollment
                                                </div>
                                               
                                            </div>
                                            <div class="gdlr-core-tab-item-content-wrap clearfix">
                                                <div class="gdlr-core-tab-item-content gdlr-core-active" data-tab-id="1" style="background-color: #f2f9ff ;;background-position: top right ;">
                                                    <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix gdlr-core-left-align gdlr-core-title-item-caption-top">
                                                        <div class="gdlr-core-title-item-title-wrap ">
                                                            <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 22px ;font-weight: 700 ;text-transform: none ;color: #314e85 ;">Why Choose Kingster?<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3>
                                                        </div>
                                                    </div>
                                                    <p>
                                                        The Kingster University Alumni Association is excited to announce the arrival of KU Alumni Connect. This is a new community building platform for Kinster’s alumni. It is the only place online where you can find, and connect with, all 90,000 Kingster’s alumni. All alumni are automatically enrolled!
                                                    </p>
                                                    <p>
                                                        Kingster University was established by John Smith in 1920 for the public benefit and it is recognized globally. Throughout our great history, Kingster has offered access to a wide range of academic opportunities. As a world leader in higher education, the University has pioneered change in the sector.
                                                    </p>
                                                </div>
                                                <div class="gdlr-core-tab-item-content " data-tab-id="2" style="background-color: #f2f9ff ;;background-position: top right ;">
                                                    <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix gdlr-core-left-align gdlr-core-title-item-caption-top">
                                                        <div class="gdlr-core-title-item-title-wrap ">
                                                            <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 22px ;font-weight: 700 ;text-transform: none ;color: #314e85 ;">Self Development<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3>
                                                        </div>
                                                    </div>
                                                    <p>
                                                        The Kingster University Alumni Association is excited to announce the arrival of KU Alumni Connect. This is a new community building platform for Kinster’s alumni. It is the only place online where you can find, and connect with, all 90,000 Kingster’s alumni. All alumni are automatically enrolled!
                                                    </p>
                                                    <p>
                                                        Kingster University was established by John Smith in 1920 for the public benefit and it is recognized globally. Throughout our great history, Kingster has offered access to a wide range of academic opportunities. As a world leader in higher education, the University has pioneered change in the sector.
                                                    </p>
                                                </div>
                                                <div class="gdlr-core-tab-item-content " data-tab-id="3" style="background-color: #f2f9ff ; ;background-position: top right ;">
                                                    <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix gdlr-core-left-align gdlr-core-title-item-caption-top">
                                                        <div class="gdlr-core-title-item-title-wrap ">
                                                            <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 22px ;font-weight: 700 ;text-transform: none ;color: #314e85 ;">Spirituality<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider"></span></h3>
                                                        </div>
                                                    </div>
                                                    <p>
                                                        The Kingster University Alumni Association is excited to announce the arrival of KU Alumni Connect. This is a new community building platform for Kinster’s alumni. It is the only place online where you can find, and connect with, all 90,000 Kingster’s alumni. All alumni are automatically enrolled!
                                                    </p>
                                                    <p>
                                                        Kingster University was established by John Smith in 1920 for the public benefit and it is recognized globally. Throughout our great history, Kingster has offered access to a wide range of academic opportunities. As a world leader in higher education, the University has pioneered change in the sector.
                                                    </p>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>






                  
                   
                  
                    
                  
                   
                 
                   
                   
                  
                </div>
            </div>
            <?php require_once 'imports/footer.php' ?>
        </div>
    </div>


    <?php require_once 'imports/js.php' ?>


</body>
</html>